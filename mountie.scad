//NOSTL

include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mcube.scad>;
include <libthread/threadlib/threadlib.scad>;

default_diameter            = 50;
default_length              = 3;    // number of hub/axles
default_height_ratio        = 1/4;  // height as a fraction of diameter
default_chamfer_ratio       = 1/8;  // chamfer as a fraction of height
default_axle_ratio          = 1/4;  // axle through-hole diameter as a fraction of overall diameter
default_spoke_length_ratio  = 3/4;  // spoke length as a fraction of available (non-chamfered) diameter
default_spoke_width_ratio   = 1/4;  // spoke width as a fraction of height
default_spoke_step          = 30;   // degrees between spokes
default_include_spokes      = true;
default_spacing_ratio       = 1.25; // multiples of diameter from center to center
default_fn                  = 180;
default_base_diameter_ratio = 2;    // size of base as a fraction of diameter
default_base_height_ratio   = 1.5;  // height_ratio (above) multiplier for base

default_camera_clip_inside_diameter = 30;
default_camera_clip_length = 30;
default_camera_clip_wall_thickness = 25.4 / 4;
default_camera_clip_closure_angle = 300;

default_thread_size = 12;
default_thread_pattern = str("M", default_thread_size);
default_bolt_scale = 0.9875;
default_bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees


module arm(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        spacing_ratio = default_spacing_ratio,
        length = default_length,
        spoke_width_ratio = default_spoke_width_ratio,
        spoke_length_ratio = default_spoke_length_ratio,

        spoke_step = default_spoke_step,

        fn = default_fn) {

    $fn = fn;    
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;

    difference() {
        
        // positive
        union() {
            color("lightblue") {

                hull() {
                    for(i = [1,length]) {
                        translate([(i - 1) * (diameter * spacing_ratio), 0, 0]) {
                            cylinder(h = height - (chamfer * 2), d = diameter                , center = true);
                            cylinder(h = height                , d = diameter - (chamfer * 2), center = true);
                        }
                    }
                }
            }
            for(i = [1:length]) {
                translate([(i - 1) * (diameter * spacing_ratio), 0, height / 2]) {
                    spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                }
            }
        }
        
        // negative
        union() {
            for(i = [1:length]) {
                translate([(i - 1) * (diameter * spacing_ratio), 0, 0]) {
                    axle(diameter, height_ratio = height_ratio, chamfer_ratio = chamfer_ratio, fn = fn);
                }
                translate([(i - 1) * (diameter * spacing_ratio), 0, -height / 2]) {
                    spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                }
            }

        }
    }
}



module axle(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio, 
        axle_ratio = default_axle_ratio,
        fn = default_fn) {

    $fn = fn;
    axle_diameter = diameter * axle_ratio;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;

    color("red") {
        cylinder(h = height, d = axle_diameter, center = true);
        for(z = [-1, 1]) {

            translate([0, 0, z * (height / 2)]) {
                cylinder(
                    h = chamfer * 2, 
                    d1 = z > 0 ? axle_diameter                 : axle_diameter + (chamfer * 4),
                    d2 = z > 0 ? axle_diameter + (chamfer * 4) : axle_diameter, 
                    center = true
                );
            }
        }
    }

}



module spokes(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio, 
        axle_ratio = default_axle_ratio,
        spoke_length_ratio = default_spoke_length_ratio,
        spoke_width_ratio = default_spoke_width_ratio,
        spoke_step = default_spoke_step,
        fn = default_fn) {
        
    $fn = fn;
    height        = diameter * height_ratio;
    axle_diameter = diameter * axle_ratio;
    chamfer       = height * chamfer_ratio;
    available_diameter = diameter - (axle_diameter + chamfer * 4);
    available_radius = available_diameter / 2;
    spoke_length  = available_radius * spoke_length_ratio;
    spoke_width   = height * spoke_width_ratio; 
    spoke_offset = (axle_diameter / 2) + chamfer + (available_radius - spoke_length) / 2;

    color("lightgreen") {


        for(i = [0:spoke_step:359]) {
            rotate([0, 0, i]) {
                translate([spoke_offset, 0, 0]) {
                    translate([spoke_length / 2, 0, 0]) {
                        rotate([45, 0, 0]) {
                            hull() {
                                cube([spoke_length - spoke_width, spoke_width, spoke_width], center = true);
                                cube([spoke_length, 0.001, 0.001], center = true);
                            }
                        }
                    }
                }
            }
        }
    }

}




module base(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        base_diameter_ratio = default_base_diameter_ratio,
        base_height_ratio = default_base_height_ratio,
        spacing_ratio = default_spacing_ratio,
        hollow = false,
        stumps = 1,
        stump_fattener = 1,
        fn = default_fn) {

    $fn = fn;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;


    // base
    if (hollow) {
        color("lightblue") {
            mtube(
                h = height * base_height_ratio,
                od = base_diameter_ratio * diameter,
                id = base_diameter_ratio * diameter - height * 2, 
                align = [0, 0, -0.5], 
                chamfer = chamfer);
        }
        color("royalblue") {
            translate([0, 0, (height * base_height_ratio) /2]) {
                mcylinder(h = height, d = base_diameter_ratio * diameter, align = [0, 0, -1], chamfer = chamfer);
            }
        }
    } else {

        color("lightblue") {
            
            hull() {
                cylinder(h = (height * base_height_ratio) - (chamfer * 2), d = (base_diameter_ratio * diameter)                , center = true);
                cylinder(h = (height * base_height_ratio) + 0.02         , d = (base_diameter_ratio * diameter) - (chamfer * 2), center = true);
            }
        }
    }

    // mounting stump
    translate([0, -(stump_fattener - 1) * height / 2, 0])
                            for(sf = [0:(stump_fattener-1) * 2]) {
                                translate([0, sf * height / 2, 0]) {
                                    union() {
        center_to_center = (stumps - 1) * 3 * height;

        translate([0, -center_to_center / 2, 0]) {
            for(s = [0 : stumps - 1]) {
                translate([0, s * height * 3, 0]) {
                        

                    translate([0, 0, (height * base_height_ratio)/2]) {
                        rotate([90, 0, 0]) {

                            stump(diameter = diameter, 
                                height_ratio = height_ratio, 
                                chamfer_ratio = chamfer_ratio,
                                spacing_ratio = spacing_ratio,
                                fn = $fn);
                                }
                            }
                            
        
                    // skirt
                    translate([0, 0, (height * base_height_ratio)/2]) {
                        color("fuchsia") hull() {
                            translate([0, 0, chamfer]) {
                                cube([diameter - (chamfer * 2), height                , 0.01], center = true);
                                cube([diameter                , height - (chamfer * 2), 0.01], center = true);
                            }
                            translate([0, 0, 0.01]) {
                                cube([diameter + (chamfer * 2), height                , 0.01], center = true);
                                cube([diameter                , height + (chamfer * 2), 0.01], center = true);
                            }
                        }
                    }
                }
            }                    
                        }
                    }

        }
    }
        
}


module stump(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        spacing_ratio = default_spacing_ratio,
        fn = default_fn,
        arm = true) {

    $fn = fn;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;



    intersection() {
        union() {
            difference(){
                color("blue") {
                    x = ((height + (diameter * spacing_ratio))) / 2;
                                translate([0, x/2, 0]) {

                    hull() {

                        cube([diameter, x , height - (chamfer * 2)], center = true);
                         cube([diameter - (chamfer * 2), x - (chamfer * 2), height], center = true);
                    }
                                        }
                }
                
                if(arm) {
                    color("red") {
                        translate([0, (height + (diameter * spacing_ratio)) / 2, 0]) {
                            cylinder(h = height + 2, d = (diameter - 0.02) - (chamfer * 2), center = true);
                        }
                    }
                }
            }
            
            if(arm) {
                translate([0, (height + (diameter * spacing_ratio)) / 2, 0]) {
                    arm(
                        diameter = diameter, 
                        height_ratio = height_ratio, 
                        chamfer_ratio = chamfer_ratio,
                        length = 1,
                        fn = fn);
                }
            }
        }    
    }
    
}



module elbow(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        fn = default_fn) {
    
    $fn = fn;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;


    // corner fillet

    union() {
        intersection() {
            color("cyan") {
                translate([0, height * 0.5, height * 0.5]) {
                    hull() {
                        cube([diameter, height * 2 - chamfer * 2, height * 2 - chamfer * 2], center = true);
                        cube([diameter - chamfer * 2, height * 2, height * 2], center = true);
                    }
                }
            }

            color("pink") {
                rotate([-45, 0, 0]) {
                    translate([0, 0, chamfer * -sin(45)]) 
                    hull() {
                        cube([diameter                , (height * 4) - (chamfer * 2), height - (chamfer * 2)], center = true);
                        cube([diameter - (chamfer * 2), (height * 4)                , height                ], center = true);
                    }
                    translate([0, 0, chamfer * 3]) 
                    hull() {
                        cube([diameter                , (height * 4) - (chamfer * 2), height - (chamfer * 2)], center = true);
                        cube([diameter - (chamfer * 2), (height * 4)                , height                ], center = true);
                    }
                }
            }
        }
    }


    
    stump(fn = fn);
    rotate([90, 0, 0]) {
        stump(
            diameter = diameter, 
            height_ratio = height_ratio, 
            chamfer_ratio = chamfer_ratio,
            fn = fn);
    }

    
}


if(is_undef(doc)) {
    echo("out of date");
}



module camera_clip(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        camera_clip_inside_diameter = default_camera_clip_inside_diameter,
        camera_clip_length = default_camera_clip_length,
        camera_clip_wall_thickness = default_camera_clip_wall_thickness,
        camera_clip_closure_angle = default_camera_clip_closure_angle,
        fn = default_fn) {
    
    od = camera_clip_inside_diameter + camera_clip_wall_thickness;
    c = camera_clip_wall_thickness / 8;
    
    angle = camera_clip_closure_angle;
    

    $fn = fn;

    translate([0, 0, 0]) {
        rotate([0, 0, 90]) {    
            color("orange") {
                mtube(h = camera_clip_length, od = od + camera_clip_wall_thickness, id = camera_clip_inside_diameter + camera_clip_wall_thickness, align = [0, 0, 0], chamfer = c, angle = camera_clip_closure_angle);

                for(z = [-1, 1]) {
                    rotate([0, 0, z * angle / 2])
                                    translate([(od + camera_clip_wall_thickness) / 2, 0, 0]) {

                        mcylinder(h = camera_clip_length, d = (od - camera_clip_inside_diameter) / 2, align = [-1, 0, 0], chamfer = c);

                    }
                }
            }
        }
    }
    
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;
    rotate([0, 90, 0]) {
        intersection() {
            difference() {
color("salmon") {                hull() {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        color("lightblue") {
                            hull() {
                                cylinder(h = height - (chamfer * 2), d = diameter                , center = true);
                                cylinder(h = height                , d = diameter - (chamfer * 2), center = true);
                            }
                        }
                    }

                    color("cyan") {
                translate([0, (od + camera_clip_wall_thickness / 2) / 2, 0]) {
                        hull() {
                            cylinder(h = height - (chamfer * 2), d = camera_clip_length                - (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0) , center = true);
                            cylinder(h = height                , d = camera_clip_length - (chamfer * 2)- (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0), center = true);
                        }
                    }
                }
                }
            }
            color("red") {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        cylinder(h = height + 2, d = (diameter - chamfer * 3), center = true);
                    }

                }

            }

            color("blue") {
                translate([0, (od / 2) + c , 0]) {
                    mcube([max(diameter, od + (camera_clip_wall_thickness)), camera_clip_inside_diameter + diameter, height], align = [0, 1, 0]);
                }
            }

        }

        translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
            arm(
                diameter = diameter, 
                height_ratio = height_ratio, 
                chamfer_ratio = chamfer_ratio,
                length = 1,
                fn = fn);
        }
        
    }

}


module washer(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        spacing_ratio = default_spacing_ratio,
        length = 1,
        spoke_width_ratio = default_spoke_width_ratio,
        spoke_length_ratio = default_spoke_length_ratio,
        spoke_step = default_spoke_step,
        axle_ratio = default_axle_ratio,
top = "female",
bottom = "female",
lock = false,
threaded = false,
thread_size = default_thread_size,
thread_pattern = default_thread_pattern, 
bolt_stretch = default_bolt_stretch,
        fn = default_fn) {

    $fn = fn;    
    chamfer       = diameter * height_ratio * chamfer_ratio;
    height        = chamfer * 4;

                                nut_turns = 6;
                                douter = diameter / 2;
                                specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
                                P = specs[0];
                                H = (nut_turns + 1) * P * bolt_stretch;
                                Dsup = specs[2];

            

    difference() {
        difference() {
            
            // positive
            union() {
                color("lightblue") {

                    hull() {
                                cylinder(h = height - (chamfer * 2), d = diameter                , center = true);
                                cylinder(h = height                , d = diameter - (chamfer * 2), center = true);
                    }
                }
                if(top == "male") {
                    translate([0, 0, height / 2]) {
                        spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                    }
                }
                if(bottom == "male") {
                    translate([0, 0, -height / 2]) {
                        spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                    }
                }

            }
            
            // negative
            union() {
                
                intersection() {
                    if(!threaded) {
                        axle(diameter, height_ratio = height_ratio/2, chamfer_ratio = chamfer_ratio, fn = fn);
                    } else {
                        if(threaded) {
                            difference() {


                                mcylinder(d = douter - 1, h = H - 1, align = [0,0,0]);

                                color("fuchsia") {
                                    translate([0, 0, -H/2]) 
                                    translate([0, 0, (P * bolt_stretch) / 2]) 
                                    scale([1, 1, bolt_stretch]) {
                                        nut(thread_pattern, turns=nut_turns, Douter=douter);
                                    }
                                }

                            }

                        }
                    }
                    if (lock) {
                        mcube([diameter * axle_ratio * 2/3, diameter * axle_ratio + 2, height + 2], align = [0, 0, 0]);
                    }
                }
                if(bottom == "female") {
                    translate([0, 0, -height / 2]) {
                        spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                    }
                }
                if(top == "female") {
                    translate([0, 0, height / 2]) {
                        spokes(diameter, height_ratio = height_ratio, spoke_step = spoke_step, spoke_width_ratio = spoke_width_ratio, spoke_length_ratio = spoke_length_ratio, chamfer_ratio = chamfer_ratio);
                    }
                }

            }
        }
    
        if(threaded) {
            union(){ 
                color("red") {
                    axle_diameter = diameter * axle_ratio;
                    translate([0, 0, height * 0.5]) {
                        mcylinder(h = chamfer * 2, d1 = Dsup - chamfer * 2, d2 = Dsup + chamfer * 2);
                    }
                    translate([0, 0, -height * 0.5]) {
                        mcylinder(h = chamfer * 2, d2 = Dsup - chamfer * 2, d1 = Dsup + chamfer * 2);
                    }

                }
            }
        }
    }
}

module mountie_nut(thread_size, thread_pattern, nut_turns, douter, bolt_stretch, chamfer) {



    module _flute(d) {
        cylinder(d = default_diameter * d, h = H, center = true, $fn = 100);
        for(z = [-1, 1]) {
            translate([0, 0, z * ((H / 2) - chamfer)]) {
                cylinder(d1 = (default_diameter * d) - (chamfer * 2 * z), d2 = (default_diameter * d) + (chamfer * 2 * z), h = chamfer * 4, center = true);
            }
        }
    }



    $fn = 100;

    specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (nut_turns + 1) * P * bolt_stretch;
        
    echo("P:", P);
    echo("H:", H);
    
    intersection() {
        union() {
            difference() {
        
                color("fuchsia") {
                    translate([0, 0, -H/2]) {
                        translate([0, 0, (P * bolt_stretch) / 2]) {
                            scale([1, 1, bolt_stretch]) {
                                nut(thread_pattern, turns=nut_turns, Douter=douter);
                            }
                        }
                    }
                }
                
                //chamfer thread hole
                for(z = [-1, 1]) {
                    d = .307;
                    translate([0, 0, z * ((H / 2) + (chamfer * 1.88))]) {
                        cylinder(d1 = (default_diameter * d) - (chamfer * 2 * z), d2 = (default_diameter * d) + (chamfer * 2 * z), h = chamfer * 4, center = true);
                    }
                }






                color("red") {
                    for(z = [0 : 60 : 359]) {
                        rotate([0, 0, z]) {
                            translate([(douter * 1.25) / 2, 0, 0]) {
                                _flute(1/6);
                            }
                        }
                    }
                }
            }
        }

        color("royalblue") {
            hull() {
                cylinder(d= douter  , h = H - chamfer * 2, center = true);
                cylinder(d= douter - chamfer * 2 , h = H , center = true);

            }
        }
    }

}






module mountie_bolt(
        reference_diameter = default_diameter,
        reference_height_ratio = default_height_ratio,
        thread_size, 
        thread_pattern, 
        bolt_turns, 
        bolt_stretch) {

    height        = reference_diameter * reference_height_ratio;
    echo("height: ", height);

    rotate([90, 0, 0]) {

        intersection() {
            color("orange") {
                scale([bolt_scale, bolt_scale, bolt_stretch]) {
                    bolt(thread_pattern, turns=bolt_turns, higbee_arc=30);
                }
            }

            color("red") {
                translate([0, 0, -height]) {
                    mcube([thread_size * bolt_scale, (thread_size * 2/3) * bolt_scale, height * bolt_turns / 2], align = [0, 0, 1]);
                }
            }
        }
    }
}




module octopi_clip(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        camera_clip_inside_diameter = default_camera_clip_inside_diameter,
        camera_clip_length = default_camera_clip_length,
        camera_clip_wall_thickness = default_camera_clip_wall_thickness,
        camera_clip_closure_angle = default_camera_clip_closure_angle,
        fn = default_fn) {
    
    od = camera_clip_inside_diameter + camera_clip_wall_thickness;
    c = camera_clip_wall_thickness / 8;
    
    angle = camera_clip_closure_angle;
    
    $fn = fn;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;


    translate([0, 0, 0]) {
        rotate([90, 0, 90]) {    
            color("orange") {
                mtube(h = height, od = od + camera_clip_wall_thickness, id = camera_clip_inside_diameter + camera_clip_wall_thickness, align = [0, 0, 0], chamfer = c);
            }
            color("salmon") {
                translate([0,0,-height / 2]) {
                    mcylinder(h = camera_clip_wall_thickness/2, d = od + camera_clip_wall_thickness/2, align = [0, 0, 1]);
                }
            }
        }
    }
    
    rotate([0, 90, 0]) {
        intersection() {
            difference() {
color("salmon") {                hull() {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        color("lightblue") {
                            hull() {
                                cylinder(h = height - (chamfer * 2), d = diameter                , center = true);
                                cylinder(h = height                , d = diameter - (chamfer * 2), center = true);
                            }
                        }
                    }

                    color("cyan") {
                translate([0, (od + camera_clip_wall_thickness / 2) / 2, 0]) {
                        hull() {
                            cylinder(h = height - (chamfer * 2), d = camera_clip_length                - (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0) , center = true);
                            cylinder(h = height                , d = camera_clip_length - (chamfer * 2)- (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0), center = true);
                        }
                    }
                }
                }
            }
            color("red") {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        cylinder(h = height + 2, d = (diameter - chamfer * 3), center = true);
                    }

                }

            }

            color("blue") {
                translate([0, (od / 2) + c , 0]) {
                    mcube([max(diameter, od + (camera_clip_wall_thickness)), camera_clip_inside_diameter + diameter, height], align = [0, 1, 0]);
                }
            }

        }

        translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
            arm(
                diameter = diameter, 
                height_ratio = height_ratio, 
                chamfer_ratio = chamfer_ratio,
                length = 1,
                fn = fn);
        }
        
    }

}


module ninja_clip(
        diameter = default_diameter, 
        height_ratio = default_height_ratio, 
        chamfer_ratio = default_chamfer_ratio,
        camera_clip_inside_diameter = default_camera_clip_inside_diameter,
        camera_clip_length = default_camera_clip_length,
        camera_clip_wall_thickness = default_camera_clip_wall_thickness,
        camera_clip_closure_angle = default_camera_clip_closure_angle,
        fn = default_fn) {
    
    od = camera_clip_inside_diameter + camera_clip_wall_thickness;
    c = camera_clip_wall_thickness / 8;
    
    angle = camera_clip_closure_angle;
    
    $fn = fn;
    height        = diameter * height_ratio;
    chamfer       = height * chamfer_ratio;


    translate([0, 0, 0]) {
        rotate([90, 0, 90]) {    
            difference()  {
            color("salmon") {
                mcube([28 + (1 * camera_clip_wall_thickness), 28 + (1 * camera_clip_wall_thickness), height], align = [0, 0, 0], chamfer = c * 2);
            }
            color("orange") {
                translate([0, 0, camera_clip_wall_thickness]) {
                    mcube([28, 28, height], align = [0, 0, 0], chamfer = 0);
                }
            }
        }
        }
    }
    
    rotate([0, 90, 0]) {
        intersection() {
            difference() {
color("salmon") {                hull() {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        color("lightblue") {
                            hull() {
                                cylinder(h = height - (chamfer * 2), d = diameter                , center = true);
                                cylinder(h = height                , d = diameter - (chamfer * 2), center = true);
                            }
                        }
                    }

                    color("cyan") {
                translate([0, (od + camera_clip_wall_thickness / 2) / 2, 0]) {
                        hull() {
                            cylinder(h = height - (chamfer * 2), d = camera_clip_length                - (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0) , center = true);
                            cylinder(h = height                , d = camera_clip_length - (chamfer * 2)- (diameter > camera_clip_length ? (diameter - camera_clip_length) / 10 : 0), center = true);
                        }
                    }
                }
                }
            }
            color("red") {
                    translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
                        cylinder(h = height + 2, d = (diameter - chamfer * 3), center = true);
                    }

                }

            }

            color("blue") {
                translate([0, (od / 2) + c , 0]) {
                    mcube([max(diameter, od + (camera_clip_wall_thickness)), camera_clip_inside_diameter + diameter, height], align = [0, 1, 0]);
                }
            }

        }

        translate([0, (diameter / 2) + (camera_clip_inside_diameter / 2) + camera_clip_wall_thickness * default_spacing_ratio, 0]) {
            arm(
                diameter = diameter, 
                height_ratio = height_ratio, 
                chamfer_ratio = chamfer_ratio,
                length = 1,
                fn = fn);
        }
        
    }

}


//render = true;
if(!is_undef(render)) {
    
    //ninja_clip(camera_clip_length = 41,camera_clip_inside_diameter = 22);
    
    base(base_diameter_ratio = 3, base_height_ratio = 3, stumps = 1, stump_fattener = 3, hollow = true);

}

