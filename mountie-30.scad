// NOSTL

include <mountie.scad>;

default_diameter = 30;

thread_size = 7;
thread_pattern = str("M", thread_size);

default_thread_size = 7;
default_thread_pattern = str("M", default_thread_size);


nut_douter = (thread_size - 5) + default_diameter - (default_diameter * default_height_ratio * default_chamfer_ratio * 2);


bolt_scale = 0.95;
bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees
bolt_turns = 24;
nut_turns = 4;
chamfer = default_diameter * default_height_ratio * default_chamfer_ratio;
