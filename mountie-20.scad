// NOSTL

include <mountie.scad>;

default_diameter = 20;
default_spoke_width_ratio = 1/3;
default_axle_ratio          = 0.3;  // axle through-hole diameter as a fraction of overall diameter

default_thread_size = 6;
default_thread_pattern = str("M", default_thread_size);

nut_douter = (default_thread_size - 5) + default_diameter - (default_diameter * default_height_ratio * default_chamfer_ratio * 2);


bolt_scale = 0.95;
bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees
bolt_turns = 24;
nut_turns = 4;
chamfer = default_diameter * default_height_ratio * default_chamfer_ratio;
