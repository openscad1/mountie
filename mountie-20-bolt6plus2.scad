include <mountie-20.scad>;
turns = 33;
stack_size = 5 + (2 * 1/2);
nuts = 2;

    specs = thread_specs(str(default_thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (turns + 1) * P * bolt_stretch;
    Rrot = specs[1];
    Dsupport = specs[2];


    stack_height = default_diameter * default_height_ratio * stack_size;

howlong = 43; // as measured: 2 nuts, 2 washers, 5 arms



echo("howlong: ", howlong);

arm_height = default_diameter * default_height_ratio;
echo("arm_height: ", default_diameter * default_height_ratio);
echo("arms_height" , arm_height * stack_size);

nut_height = (nut_turns + 1) * P * bolt_stretch;
echo("nut_height: ", nut_height);
echo("nuts_height: ", nut_height * nuts);
calculated_height = arm_height * stack_size + nut_height * nuts;
echo("calculated_height: ", calculated_height);




mountie_bolt(
    reference_diameter = default_diameter,
    reference_height_ratio = default_height_ratio,
    thread_size = default_thread_size, 
    thread_pattern = default_thread_pattern, 
    bolt_turns = turns, 
    bolt_stretch = bolt_stretch);



//render_bolt = true;
if(!is_undef(render_bolt)) {

    specs = thread_specs(str(default_thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (turns + 1) * P * bolt_stretch;
    Rrot = specs[1];
    Dsupport = specs[2];

    echo("P (pitch):", P);
    echo("H:", H);
    echo("Rrot:", Rrot);
    echo("Dsupport:", Dsupport);
    


    translate([0, (P * bolt_stretch) /2 , 0]) {
        color("green")   mcube([25,      calculated_height, 1], align = [ 0, -1,  0]); // measuring stick
    }
}

