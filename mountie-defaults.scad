//NOSTL

default_diameter            = 50;
default_length              = 3;    // number of hub/axles
default_height_ratio        = 1/4;  // height as a fraction of diameter
default_chamfer_ratio       = 1/8;  // chamfer as a fraction of height
default_axle_ratio          = 1/4;  // axle through-hole diameter as a fraction of overall diameter
default_spoke_length_ratio  = 3/4;  // spoke length as a fraction of available (non-chamfered) diameter
default_spoke_width_ratio   = 1/4;  // spoke width as a fraction of height
default_spoke_step          = 30;   // degrees between spokes
default_include_spokes      = true;
default_spacing_ratio       = 1.25; // multiples of diameter from center to center
default_fn                  = 180;
default_base_diameter_ratio = 2;    // size of base as a fraction of diameter
default_base_height_ratio   = 1.5;  // height_ratio (above) multiplier for base

default_camera_clip_inside_diameter = 30;
default_camera_clip_length = 30;
default_camera_clip_wall_thickness = 25.4 / 4;
default_camera_clip_closure_angle = 300;

default_thread_size = 12;
default_thread_pattern = str("M", default_thread_size);
default_bolt_scale = 0.9875;
default_bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees


