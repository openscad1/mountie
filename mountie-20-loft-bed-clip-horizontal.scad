include <mountie-20.scad>;

gap = 23.75;

translate([0, 0, 10]) {
    rotate([0, 0, 90]) {
        arm(length = 2, spacing_ratio = (default_diameter + gap) / default_diameter);
    }
}

/*

translate([0,  default_diameter + gap, 0]) arm(length = 2, spacing_ratio = default_spacing_ratio * 3);
translate([0, 0, 0]) arm(length = 2, spacing_ratio = default_spacing_ratio * 2);

color("blue") {
    translate([0, default_diameter/2, 0]) {
        mcube([10, gap, 30], align = [0, 1, 0]);
    }
}

*/