include <mountie-20.scad>;


mountie_nut(default_thread_size, default_thread_pattern, nut_turns, nut_douter * 0.5, bolt_stretch, chamfer);


/*  
    specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (nut_turns + 1) * P * bolt_stretch;
        
    echo("P:", P);
    echo("H:", H);

    cube([15,15,10.9375], center = true);
*/
