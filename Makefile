include libmake/Makefile
all : openscad.all
clean : openscad.clean
tidy : openscad.tidy

fc find_customizers :
	@egrep -o '// *\[([0-9]+, *)*[0-9]+ *\] *$$' *.scad
