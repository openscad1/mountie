include <mountie-50.scad>;

turns = 43.25;
stack_size = 6;
nuts = 2;

    stack_height = default_diameter * default_height_ratio * stack_size;

howlong = 11 * nuts + stack_height;
echo("howlong: ", howlong);

    mountie_bolt(
        reference_diameter = default_diameter,
        reference_height_ratio = default_height_ratio,
        thread_size = thread_size, 
        thread_pattern = thread_pattern, 
        bolt_turns = turns, 
        bolt_stretch = bolt_stretch);



//render_bolt = true;
if(!is_undef(render_bolt)) {

    echo("stack_height: ", stack_height);


        specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
        P = specs[0];
        H = (turns + 1) * P * bolt_stretch;
            
        echo("P:", P);
        echo("H:", H);

    color("blue")    mcube([ 5,            H, 5], align = [ 2, -1,  0]); // number of "standard discs" long

    translate([0, -11, 0])
    color("fuchsia") mcube([ 5, stack_height, 5], align = [-2, -1,  0]); // length of threads
    
    translate([0, P/2 +.25, 0])
    color("green")   mcube([25,      howlong, 1], align = [ 0, -1,  0]); // measuring stick
}

