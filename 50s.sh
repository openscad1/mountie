echo make tidy
ls mountie-50-* \
  | sed 's/scad$/stl/' \
  | sed 's,^,make openscad.artifacts/,'
