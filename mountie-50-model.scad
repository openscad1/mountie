include <mountie-50.scad>;

$fn = default_fn;

// arm/bolt/nut cutaway
//

difference(){

    union() {
        color("green")
        translate([0, 0, default_diameter / 4 + -.6])
        mountie_nut(default_thread_size, default_thread_pattern, nut_turns, nut_douter * 0.5, bolt_stretch, chamfer);

        color("blue")
        translate([0, 0, default_diameter / 2])
        rotate([90, 0, 0])
            mountie_bolt(
                reference_diameter = default_diameter,
                reference_height_ratio = default_height_ratio,
                thread_size = default_thread_size, 
                thread_pattern = default_thread_pattern, 
                bolt_turns = bolt_turns, 
                bolt_stretch = bolt_stretch);
        
        color("fuchsia") 
        translate([0, 0, -default_diameter /4])
        arm(length = 2);

    }
    mcube([default_diameter * 2, default_diameter * 2, default_diameter * 2], align = [0, -1, 0]);
}


// cable cookie model
//
translate([default_diameter * default_spacing_ratio * 2, 0, 0]) {

    up = -1;

    difference() {

        arm(length = 1);

        translate([0, 0, 2 * up]) {
            union() {
                color("red") {
                    for(z = [0 : 30 : 359]) {
                        rotate([0, 0, z])
                        translate([14, 0, 0]) mcube([20, 5, 20], align=[1, 0, -1 * up]);
                    }
                }

                color("orange") mtube(od = default_diameter * default_axle_ratio + 27.5, id = default_diameter * default_axle_ratio + 15, h = 20, align = [0, 0, -1 * up]);

            }
        }
    }

    translate([0, 0, default_diameter * default_height_ratio]) {
        arm(length = 1);
    }
}
