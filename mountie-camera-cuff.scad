include <mountie-30.scad>;

include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mcube.scad>;

module camera_cuff(angle = default_camera_clip_closure_angle, h = default_camera_clip_length, id = default_camera_clip_inside_diameter, od = 30 + default_camera_clip_wall_thickness, fn = default_fn) {

    c = (od - id) / 8;

    $fn = fn;

    delta = od - id;


    difference() {
        union() {
            color("lightgreen") {
                mtube(h = h, od = od, id = id, align = [0, 0, 0], chamfer = c, angle = angle);
                for(z = [-1, 1]) {
                    rotate([0, 0, z * angle / 2])
                        translate([od / 2, 0, 0]) {
                        mcylinder(h = h, d = (od - id) / 2, align = [-1, 0, 0], chamfer = c);

                    }
                }
            }


            
            color("red") {
                for(z = [-1, 1]) {
                    translate([0, 0, z * ((default_camera_clip_length + default_camera_clip_wall_thickness/2) / 2 - c)]) {
                        mtube(h = default_camera_clip_wall_thickness / 2, od = od + c*2, id = id - c*2, align = [0, 0, 0], chamfer = c, angle = angle);
                        for(z = [-1, 1]) {
                            rotate([0, 0, z * angle / 2]) {
                                translate([od / 2 + c, 0, 0]) {
                                    mcylinder(h = default_camera_clip_wall_thickness / 2, d = (od - id) / 2 + (c * 2), align = [-1, 0, 0], chamfer = c);
                                    }

                            }
                        }
                    }
                }
            }
        }
        
        union() {
            color("pink") {
                mcylinder(h = h  + default_camera_clip_wall_thickness * 2, d = id + 0.02);
            }
        }
    }
}


camera_cuff();

