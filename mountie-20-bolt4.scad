include <mountie-20.scad>;


    mountie_bolt(
        reference_diameter = default_diameter,
        reference_height_ratio = default_height_ratio,
        thread_size = default_thread_size, 
        thread_pattern = default_thread_pattern, 
        bolt_turns = 40, 
        bolt_stretch = bolt_stretch);


//color("blue") mcube([5, 57.5 + (12.5 / 1), 5], align = [2, -1, 0]);
