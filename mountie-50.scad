// NOSTL

include <mountie.scad>;

default_diameter = 50;

nut_douter = default_diameter - (default_diameter * default_height_ratio * default_chamfer_ratio * 2);

thread_size = 12;
thread_pattern = str("M", thread_size);
bolt_scale = 0.9875;
bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees
bolt_turns = 24;
nut_turns = 4;
chamfer = default_diameter * default_height_ratio * default_chamfer_ratio;

