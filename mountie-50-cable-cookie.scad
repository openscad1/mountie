include <mountie-50.scad>;

$fn = default_fn;

up = -1;

difference() {

    arm(length = 1);

    translate([0, 0, 2 * up]) {
        union() {
            color("red") {
                for(z = [0 : 30 : 359]) {
                    rotate([0, 0, z])
                    translate([14, 0, 0]) mcube([20, 5, 20], align=[1, 0, -1 * up]);
                }
            }

            color("orange") mtube(od = default_diameter * default_axle_ratio + 27.5, id = default_diameter * default_axle_ratio + 15, h = 20, align = [0, 0, -1 * up]);
            
        }
    }
}